let arrayHistorias = []
let seccionHistoria = document.getElementById('history-section')
async function getStories() {
await fetch('https://gkfibffviwvmphzqvuqe.supabase.co/rest/v1/stories?select=*', {
    method: 'GET',
    mode: 'cors',
    headers: {
        'Content-Type': 'application/json',
        'apikey': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6ImdrZmliZmZ2aXd2bXBoenF2dXFlIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTA5ODQ5NTgsImV4cCI6MjAyNjU2MDk1OH0.M--1JO0f0zos59CcBc8oCPKZmz2su3qx0Z2hOqQK9c0',
        'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6ImdrZmliZmZ2aXd2bXBoenF2dXFlIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTA5ODQ5NTgsImV4cCI6MjAyNjU2MDk1OH0.M--1JO0f0zos59CcBc8oCPKZmz2su3qx0Z2hOqQK9c0'
    },
})
    .then( async (response) => {
        //TODO Make a conditional from status response
        await response.json().then(r => {
            r.forEach(storie => {
                //TODO Add function formatter
                arrayHistorias.push(storie)
            })
        })
    })
    .catch(() => {
        //TODO Alert
    })
    //TODO Make it work with de fetch data
for (let i = 0; i < arrayHistorias.length; i++) {
    seccionHistoria.innerHTML += `
        <div style="cursor: pointer; display: flex; flex-direction: column; align-items: center">
            <div style="padding: 6px;">
                <div class="${arrayHistorias[i].isUpToDate ? 'border-stories-disabled' : 'border-stories-active'}" >
                    <div style="background-color: white; border-radius: 60px; display: flex; align-items: center; justify-content: center; padding: 3px; position: relative; height: 60px; width: 60px; border: 2px solid white">
                    <img
                            style="height: 60px; width: 60px; border-radius: 60px;"
                            src="${arrayHistorias[i].img}"
                            alt="profile-photo">
                    </div>
                </div>
            </div>
            <div>
                <span style="font-family: sans-serif; font-size: 12px;">${arrayHistorias[i].userName}</span>
            </div>
        </div>
    `;
}}

getStories()